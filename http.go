package mason_go_sdk

import (
	"bytes"
	"context"
	"fmt"
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
	"net/http"
	"net/url"
)

type httpRequestConfig struct {
	et     mason_client.EnvironmentType
	path   string
	method string
	body   []byte
	params url.Values
}

func (m *Mason) generateHttpRequest(ctx context.Context, cfg httpRequestConfig) (*http.Request, error) {
	ep := fmt.Sprintf("%s/%s", string(m.client.GetBasePath(cfg.et)), cfg.path)
	req, err := http.NewRequestWithContext(ctx, cfg.method, ep, bytes.NewReader(cfg.body))
	if err != nil {
		return nil, err
	}

	if cfg.params.Encode() != "" {
		req.URL.RawQuery = cfg.params.Encode()
	}

	return req, nil
}
