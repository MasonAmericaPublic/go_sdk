package mason_client

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"

	log "github.com/sirupsen/logrus"
)

const sdkVersion = "0.0.12"

type EnvironmentType string

const (
	Platform  EnvironmentType = "platform"
	WebSocket EnvironmentType = "websocket"
)

type MasonHttpClient struct {
	Endpoints          Endpoints
	APIKey             string
	HttpClient         *http.Client
	consumerUserAgents []userAgent
}

func NewMasonClient(cfg Config) (*MasonHttpClient, error) {
	if cfg.APIKey == "" {
		return nil, errors.New("missing auth token, use --api-key or set the MASON_API_KEY environment variable")
	}

	return &MasonHttpClient{
		Endpoints:          cfg.Endpoints,
		APIKey:             cfg.APIKey,
		HttpClient:         &http.Client{},
		consumerUserAgents: make([]userAgent, 0),
	}, nil
}

func (c *MasonHttpClient) GetBasePath(et EnvironmentType) Endpoint {
	if endpoint, ok := c.Endpoints[et]; ok {
		return endpoint
	}

	panic(fmt.Sprintf("cant find Endpoint for environment type %s", et))
}

func (c *MasonHttpClient) setAuthHeader(req *http.Request) error {
	if c.APIKey != "" {
		req.Header.Add("authorization", fmt.Sprintf("BASIC %s", c.APIKey))
		return nil
	}
	return errors.New("missing authentication")
}

func dumpRequest(req *http.Request) {
	if log.GetLevel() == log.DebugLevel {
		body, err := httputil.DumpRequest(req, false)
		if err != nil {
			log.Error(err)
			return
		}
		log.Debug(string(body))
	}
}

func dumpResponse(res *http.Response) {
	if log.GetLevel() == log.DebugLevel {
		body, err := httputil.DumpResponse(res, true)
		if err != nil {
			log.Error(err)
			return
		}
		log.Debug(string(body))
	}
}

func (c *MasonHttpClient) Do(
	req *http.Request,
	result interface{},
	errMessage interface{}) error {
	_, err := c.DoRequest(req, result, errMessage, true)
	return err
}

// TODO, instead of shouldDecodeResponse, refactor this into passing a decode function
func (c *MasonHttpClient) DoRequest(
	req *http.Request,
	result interface{},
	errMessage interface{},
	shouldDecodeResponse bool) ([]byte, error) {

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	c.setUserAgents(req)

	err := c.setAuthHeader(req)
	if err != nil {
		return nil, err
	}

	dumpRequest(req)

	res, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	dumpResponse(res)

	decoder := json.NewDecoder(res.Body)
	// TODO this section seems off
	if shouldDecodeResponse {
		if res.StatusCode >= 200 && res.StatusCode < 300 {
			err = decoder.Decode(result)
			if err != nil {
				return nil, err
			}
			return nil, nil
		}
	} else {
		if res.StatusCode >= 200 && res.StatusCode < 300 {
			return io.ReadAll(res.Body)
		}
	}

	if errMessage != nil {
		err = decoder.Decode(errMessage)
		if err != nil {
			return nil, err
		}
	}

	return nil, fmt.Errorf("request failed with status code %d", res.StatusCode)
}

func (c *MasonHttpClient) DoRaw(req *http.Request, result interface{}) error {
	dumpRequest(req)

	res, err := c.HttpClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	dumpResponse(res)

	if res.StatusCode >= 200 && res.StatusCode < 300 {
		if result != nil {
			decoder := json.NewDecoder(res.Body)
			err := decoder.Decode(result)
			if err != nil {
				return err
			}
		}
		return nil
	}

	return fmt.Errorf("request failed with status code %d", res.StatusCode)
}

// User agent formatting programmed to match the user agent header standard
type userAgent struct {
	product string
	version string
	comment *string
}

func (ua userAgent) formatUserAgent() string {
	userAgentEntry := fmt.Sprintf("%s/%s", ua.product, ua.version)
	if ua.comment != nil {
		userAgentEntry = fmt.Sprintf("%s (%s)", userAgentEntry, *ua.comment)
	}
	return userAgentEntry
}

func (c *MasonHttpClient) AddUserAgent(product string, version string, comment *string) {
	c.consumerUserAgents = append(c.consumerUserAgents, userAgent{
		product: product,
		version: version,
		comment: comment,
	})
}

const MasonGoSdkUserAgent = "Mason-go-sdk"

func (c *MasonHttpClient) setUserAgents(req *http.Request) {
	requestUserAgent := req.UserAgent()
	goSdkUserAgentEntry := userAgent{product: MasonGoSdkUserAgent, version: sdkVersion}.formatUserAgent()
	if requestUserAgent == "" {
		requestUserAgent = goSdkUserAgentEntry
	} else {
		requestUserAgent = fmt.Sprintf("%s %s", requestUserAgent, goSdkUserAgentEntry)
	}
	for _, ua := range c.consumerUserAgents {
		requestUserAgent = fmt.Sprintf("%s %s", requestUserAgent, ua.formatUserAgent())
	}
	req.Header.Add("User-Agent", requestUserAgent)
}
