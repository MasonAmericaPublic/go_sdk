package mason_client

import (
	"fmt"
	"net/url"
)

const bymasonDomain = "api.bymason.com"

type Endpoint string
type Endpoints map[EnvironmentType]Endpoint

func buildWebsocketUrl(domain string) (string, error) {
	return buildUrl(domain, "wss")
}

func buildPlatformUrl(domain string) (string, error) {
	return buildUrl(domain, "https")
}

func buildUrl(domain, protocol string) (string, error) {
	var normalizedDomain = bymasonDomain
	if domain != "" {
		normalizedDomain = domain
	}

	url, err := url.Parse(fmt.Sprintf("%s://%s", protocol, normalizedDomain))
	if err != nil {
		return "", err
	}

	return url.String(), nil
}
