package mason_go_sdk

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	p "path"
	"strconv"

	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/xray"
)

const (
	xrayPath string = "v1/global/xray"
)

func (m *Mason) ListFiles(ctx context.Context, deviceID, path string) (*xray.ListFilesResponse, error) {
	body := map[string]interface{}{"path": path}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	ep := p.Join(xrayPath, deviceID, "list-files")

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		body:   jsonBody,
	})
	if err != nil {
		return nil, err
	}

	result := xray.ListFilesResponse{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("request failed with error: %s, message: %s", errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}

func (m *Mason) GetFile(ctx context.Context, deviceID, path string) ([]byte, error) {
	body := map[string]interface{}{"path": path}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	ep := p.Join(xrayPath, deviceID, "get-file")

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		body:   jsonBody,
	})
	if err != nil {
		return nil, err
	}

	errorResponse := ErrorResponse{}
	result, err := m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("request failed with error: %s, message: %s", errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return result, nil
}

func (m *Mason) PutFile(ctx context.Context, deviceID, path string, fileContents []byte, fileName string) error {
	ep := p.Join(xrayPath, deviceID, "put-file")

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", fileName)
	if err != nil {
		return err
	}
	_, err = part.Write(fileContents)
	if err != nil {
		return err
	}

	_ = writer.WriteField("path", path)
	err = writer.Close()
	if err != nil {
		return err
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		body:   body.Bytes(),
	})
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", writer.FormDataContentType())

	errorResponse := ErrorResponse{}
	_, err = m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return err
	}
	return nil
}

func (m *Mason) DeleteFile(ctx context.Context, deviceID, path string) error {
	body := map[string]interface{}{"path": path}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	ep := p.Join(xrayPath, deviceID, "delete-file")

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		body:   jsonBody,
	})
	if err != nil {
		return err
	}

	errorResponse := ErrorResponse{}
	_, err = m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return err
	}
	return nil
}

func (m *Mason) DescribeLogBuffers(ctx context.Context, deviceID, buffer string) (*xray.DescribeLogBuffersResponse, error) {
	ep := p.Join(xrayPath, deviceID, "describe-log-buffers")

	v := url.Values{}
	v.Add("buffer", buffer)

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodGet,
		params: v,
	})
	if err != nil {
		return nil, err
	}

	result := xray.DescribeLogBuffersResponse{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}

func (m *Mason) GetLogs(ctx context.Context, deviceID string, params xray.GetLogsParameters) (*xray.GetLogsResponse, error) {
	ep := p.Join(xrayPath, deviceID, "get-logs")

	v := url.Values{}
	if params.Buffer != "" {
		v.Add("buffer", params.Buffer)
	}
	if params.Level != "" {
		v.Add("level", params.Level)
	}
	if params.Count > 0 {
		v.Add("count", strconv.Itoa(params.Count))
	}
	if params.Pid > 0 {
		v.Add("pid", strconv.Itoa(params.Pid))
	}
	if params.StartTime != "" {
		v.Add("startTime", params.StartTime)
	}
	if len(params.Tags) > 0 {
		for _, tag := range params.Tags {
			v.Add("tags", tag)
		}
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodGet,
		params: v,
	})
	if err != nil {
		return nil, err
	}

	result := xray.GetLogsResponse{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}

func (m *Mason) InstallApk(ctx context.Context, parameters xray.InstallApkParameters) error {
	ep := p.Join(xrayPath, parameters.DeviceId, "install-apk")

	v := url.Values{}
	v.Add("apkName", parameters.ApkName)
	v.Add("reinstall", strconv.FormatBool(parameters.Reinstall))
	v.Add("allowTestApk", strconv.FormatBool(parameters.AllowTestApk))
	v.Add("allowVersionDowngrade", strconv.FormatBool(parameters.AllowVersionDowngrade))
	v.Add("grantRuntimePermissions", strconv.FormatBool(parameters.GrantRuntimePermissions))

	f, err := os.Open(parameters.FilePath)
	if err != nil {
		return err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		body:   data,
		params: v,
	})
	if err != nil {
		return err
	}

	errorResponse := ErrorResponse{}
	_, err = m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return err
	}
	return nil
}

func (m *Mason) UninstallApk(ctx context.Context, deviceId, apkName string) error {
	ep := p.Join(xrayPath, deviceId, "uninstall-apk")

	v := url.Values{}
	v.Add("apkName", apkName)

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodPost,
		params: v,
	})
	if err != nil {
		return err
	}

	errorResponse := ErrorResponse{}
	_, err = m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return err
	}
	return nil
}

func (m *Mason) GetBugReport(ctx context.Context, deviceID string) ([]byte, error) {
	ep := p.Join(xrayPath, deviceID, "get-bug-report")

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodGet,
	})
	if err != nil {
		return nil, err
	}

	errorResponse := ErrorResponse{}
	result, err := m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return result, nil
}

func (m *Mason) GetScreenCapture(ctx context.Context, deviceID string) ([]byte, error) {
	ep := p.Join(xrayPath, deviceID, "get-screen-capture")

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodGet,
	})
	if err != nil {
		return nil, err
	}

	errorResponse := ErrorResponse{}
	result, err := m.client.DoRequest(req, nil, &errorResponse, false)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return result, nil
}
