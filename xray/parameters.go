package xray

type InstallApkParameters struct {
	DeviceId                string
	ApkName                 string
	Reinstall               bool
	AllowTestApk            bool
	AllowVersionDowngrade   bool
	GrantRuntimePermissions bool
	FilePath                string
}

type GetLogsParameters struct {
	Buffer    string
	Level     string
	Count     int
	Pid       int
	StartTime string
	Tags      []string
}

type LogCatParameters struct {
	Buffer    string
	Level     string
	Count     string
	Pid       string
	StartTime string
	Tags      []string
}
