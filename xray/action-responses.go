package xray

type DescribeLogBuffersResponse struct {
	Version string              `json:"version"`
	Buffers []BufferDescription `json:"buffers"`
}

type BufferDescription struct {
	Buffer              string      `json:"buffer"`
	MaxSizeInBytes      int         `json:"maxSizeInBytes"`
	CurrentSizeInBytes  int         `json:"currentSizeInBytes"`
	CurrentEntryCount   int         `json:"currentEntryCount"`
	MaxEntrySizeBytes   int         `json:"maxEntrySizeBytes"`
	MayPayloadSizeBytes int         `json:"maxPayloadSizeBytes"`
	Chattiest           []Chattiest `json:"chattiest"`
}

type Chattiest struct {
	Pid         int    `json:"pid"`
	Uid         int    `json:"uid"`
	PackageName string `json:"packageName"`
	SizeInBytes int    `json:"sizeBytes"`
}

type GetLogsResponse struct {
	Version string   `json:"version"`
	Buffer  string   `json:"buffer"`
	Entries []string `json:"entries"`
}

type ListFilesResponse struct {
	Version     string   `json:"version"`
	Files       []string `json:"files"`
	Directories []string `json:"directories"`
}

type GetFileResponse struct {
	File []byte
}
