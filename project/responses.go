package project

import "time"

// Project is a collection of versioned Mason OS configurations at a pinned version
type Project struct {
	// API Level is an integer value that uniquely identifies the framework API
	// revision offered by a version of the Android platform
	APILevel APILevel `json:"apiLevel"`
	// CreatedAt is the timestamp the Project got created.
	CreatedAt time.Time `json:"createdAt"`
	// UpdatedAt is the timestamp the Project was last updated.
	UpdatedAt time.Time `json:"updatedAt"`
	// Description is used to represent how to describe a Project.
	Description string `json:"description"`
	// DeviceFamily corresponds to the device type D450, G450, and so on.
	DeviceFamily *DeviceFamily `json:"deviceFamily"`
	// Name is the unique human readable Project identifier
	Name string `json:"name"`
	// Id is the unique Project identifier
	Id string `json:"id"`
}

// API Level is an integer value that uniquely identifies the framework API
// revision offered by a version of the Android platform
type APILevel int

const (
	APILevel_23 APILevel = 23
	APILevel_25 APILevel = 25
	APILevel_27 APILevel = 27
	APILevel_30 APILevel = 30
)

func (al APILevel) IsValid() bool {
	switch al {
	case APILevel_23, APILevel_25, APILevel_27, APILevel_30:
		return true
	}
	return false
}

// DeviceFamily corresponds to the device type D450, G450, and so on.
type DeviceFamily string

const (
	D03    DeviceFamily = "D03"
	F07    DeviceFamily = "F07"
	D450   DeviceFamily = "D450"
	D450A  DeviceFamily = "D450A"
	D450B  DeviceFamily = "D450B"
	C210   DeviceFamily = "C210"
	F210   DeviceFamily = "F210"
	F450   DeviceFamily = "F450"
	G430   DeviceFamily = "G430"
	G450   DeviceFamily = "G450"
	I3399A DeviceFamily = "I3399A"
	A4100  DeviceFamily = "A4100"
	D215   DeviceFamily = "D215"
)

func (df DeviceFamily) IsValid() bool {
	switch df {
	case D03, F07, D450, D450A, D450B, C210, F210, F450, G430, G450, I3399A, A4100, D215:
		return true
	}
	return false
}
