# Mason Golang SDK

This is the official Golang SDK for interacting with the [Mason Platform](https://bymason.com/).

# Getting started

Add the SDK to your project:
```bash
go get gitlab.com/MasonAmericaPublic/go-sdk
```

## Authentication

The SDK authenticates all requests using API keys. To configure the SDK, either set your API key as an environment variable, or pass it in your configuration.

If set as an environment variable, the SDK will check for `MASON_API_KEY`.

## Basic example

Below is a basic example setting up the SDK.

```go
package main

import (
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
)

func main() {
	mason := masonGo.Mason{}
	var apikey = "" // an empty apikey will read the MASON_API_KEY env var
	err := masonGo.NewMason(&mason, apikey)
	if err != nil {
		panic(err)
	}
}
```

To build and run the above program:

```bash
go build -o mason .
MASON_API_KEY=your-api-key ./mason
```