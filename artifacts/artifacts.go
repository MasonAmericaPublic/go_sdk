package artifacts

// ArtifactType represents the type of artifact. Artifacts can be any file that can be used to build an OS, including configurations, APKs, and images.
type ArtifactType string

const (
	// APKType is an Android Package.
	APKType ArtifactType = "apk"
	// ConfigType is a Mason Configuration. This is a description of how the operating system should be configured.
	ConfigType ArtifactType = "config"
	// MediaType is any image that can be used by a Configuration. These are used as bootanimations or splash screens.
	MediaType         ArtifactType = "media"
	BootAnimationType ArtifactType = "bootanimation"
	SplashType        ArtifactType = "splash"
)
