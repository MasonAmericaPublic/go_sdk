package artifacts

import (
	"time"
)

// Artifact is any artifact registered with the platform. This encompasses all Configurations, images, and APKs.
type Artifact struct {
	Version   string    `json:"version"`
	CreatedAt time.Time `json:"createdAt"`
	Customer  string    `json:"customer"`
	Checksum  struct {
		Sha1 string `json:"sha1"`
	} `json:"checksum"`
	URL       string       `json:"url"`
	Composite string       `json:"composite"`
	Name      string       `json:"name"`
	Type      ArtifactType `json:"type"`
}

type Checksum struct {
	Sha1 string `json:"sha1"`
}
