package deploy

import "time"

// A Deployment is an artifact that will be installed on every device
// in a deployment group.
type Deployment struct {
	Id             string           `json:"id"`
	GroupId        string           `json:"groupId"`
	CreatedAt      time.Time        `json:"createdAt"`
	UpdatedAt      time.Time        `json:"updatedAt"`
	DeployInsecure bool             `json:"deployInsecure"`
	Artifact       DeployedArtifact `json:"artifact"`
}

// DeployedArtifact is a name/version/type tuple representing the artifact deployed.
type DeployedArtifact struct {
	Name    string         `json:"name"`
	Version string         `json:"version"`
	Type    DeploymentType `json:"type"`
}

// DeploymentType is an enum of the different types of deployments.
type DeploymentType string

const (
	// DeploymentTypeAPK represents an APK deployment. Multiple APK deployments
	// can be active at one time as long as they have different package names.
	// Newer deployments for the same package name will replace older ones.
	DeploymentTypeAPK DeploymentType = "apk"
	// DeploymentTypeOTA represents an OTA deployment. OTA deployments
	// override each other. Newer deployments will replace older ones.
	// Devices cannot downgrade to a lower version.
	DeploymentTypeOTA DeploymentType = "ota"
	// DeploymentTypeConfig represents a config deployment. Config deployments
	// override each other. Newer deployments will replace older ones.
	// Device can downgrade to a lower version or change projects. If they
	//downgrade or change projects, they will wipe their data first.
	DeploymentTypeConfig DeploymentType = "config"
)
